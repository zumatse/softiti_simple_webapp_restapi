'use strict';
process.env.NODE_ENV = 'test';

const Pet = require('../models/Pet');
const chai = require('chai');
const chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
let defaultPet = {
  "name": "Filemon",
  "gender": "Male",
  "age": 20,
  "id": 74,
  "animal": "Cat"
}

chai.use(chaiHttp);

describe('Pets', () => {
  beforeEach((done) => {
    Pet.remove({}, (err) => {
      done();
    });
  });

  describe('/GET pets', () => {
    it('it should GET all the pets', (done) => {
      chai.request(server)
        .get('/pets/data')
        .end((err, res)=> {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST pet', () => {
    it('it should not POST a pet without name', (done) => {
      let pet = {
        "animal": "Cat",
        "gender": "Male",
        "age": 20
      };
      chai.request(server)
        .post('/pets')
        .send(pet)
        .end((err, res) => {
          res.should.have.status(206);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('name');
          res.body.errors.name.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a pet without gender', (done) => {
      let pet = {
        "animal": "Cat",
        "name": "Filemon",
        "age": 20
      };
      chai.request(server)
        .post('/pets')
        .send(pet)
        .end((err, res) => {
          res.should.have.status(206);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('gender');
          res.body.errors.gender.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a pet without age', (done) => {
      let pet = {
        "animal": "Cat",
        "gender": "Male",
        "name": "Filemon"
      };
      chai.request(server)
        .post('/pets')
        .send(pet)
        .end((err, res) => {
          res.should.have.status(206);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('age');
          res.body.errors.age.should.have.property('kind').eql('Number');
          done();
        });
    });
    it('it should not POST a pet without animal type', (done) => {
      let pet = {
        "name": "Filemon",
        "gender": "Male",
        "age": 20
      };
      chai.request(server)
        .post('/pets')
        .send(pet)
        .end((err, res) => {
          res.should.have.status(206);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('animal');
          res.body.errors.animal.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should POST a pet', (done) => {
      let pet = defaultPet;
      chai.request(server)
        .post('/pets')
        .send(pet)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('createdPet');
          res.body.createdPet.should.have.property('age');
          res.body.createdPet.should.have.property('gender');
          res.body.createdPet.should.have.property('animal');
          res.body.createdPet.should.have.property('name');
          done();
        });
    });
  });
  describe('GET pets/:id pet', () => {
    it('it should GET a pet by the given id', (done) => {
      let pet = new Pet(defaultPet);
      pet.save()
        .then(pet => {
          chai.request(server)
            .get(`/pets/${pet.id}`)
            .send(pet)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('message');
              res.body.message[0].should.have.property('id');
              res.body.message[0].should.have.property('name');
              res.body.message[0].should.have.property('animal');
              res.body.message[0].should.have.property('gender');
              done();
            });
        });
    });
  });
  describe('PATCH pets/:id', () => {
    it('it should update a pet with given ID', (done) => {
        let pet = new Pet(defaultPet);
        let updatedPet = {
          "name": "Reksio",
          "gender": "Male",
          "age": 20,
          "animal": "Dog"
        };
        pet.save((err, pet) => {
          chai.request(server)
            .patch(`/pets/${pet.id}`)
            .send(updatedPet)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('nModified').eql(1);
              res.body.should.have.property('ok').eql(1);
              done();
            });
        });
    });
  });
  describe('DELETE /pets/:id', () => {
    it('it should delete a pet with a given id from database', (done) => {
      let pet = new Pet(defaultPet);
      pet.save((err, pet) => {
        chai.request(server)
          .delete(`/pets/${pet.id}`)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Animal deleted from database');
            done();
          });
      });
    });
  });
});



