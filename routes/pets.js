'use strict';
const express = require('express');
const router = express.Router();
const path = require('path');
const mongoose = require('mongoose');
const pets = require('../controllers/pets');

mongoose.Promise = global.Promise;

router.get('/', (req, res) => {
  res.status(200);
  res.sendFile(path.resolve('./views/table.html'));
});

router.get('/data', pets.getPetsData);

router.get('/:id', pets.getPetById);

router.post('/', pets.postPet);

router.patch('/:id', pets.updatePet);

router.delete('/:id', pets.deletePet);

module.exports = router;