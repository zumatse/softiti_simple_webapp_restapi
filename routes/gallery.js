'use strict';
const express = require('express');
const router = express.Router();
const path = require('path');


router.get('/', (req, res) => {
  res.sendFile(path.resolve('./views/gallery.html'));
});

module.exports = router;