"use strict";

document.addEventListener("DOMContentLoaded", function() {
  const table = document.querySelector('table');
  const tbody = document.querySelector("tbody");
  const form = document.querySelector('.editRow');
  const addBtn = document.querySelector('.addBtn');
  const header = document.querySelector('thead');
  let currentRow;
  let reverse = true;
  let lastId;
  let animals;

  const createButtons = () => {
    let td = document.createElement('td'),
        editBtn = document.createElement('i'),
        deleteBtn = document.createElement('i');
    editBtn.className = "fas fa-pencil-alt edit";
    deleteBtn.className = "fas fa-trash-alt delete";
    td.appendChild(editBtn);
    td.appendChild(deleteBtn);
    return td;
  };

  const createRow = (pet) => {
    let row = document.createElement('tr'),
          buttons = createButtons(),
          index = document.createElement('td'),
          animal = document.createElement('td'),
          name = document.createElement('td'),
          gender = document.createElement('td'),
          age = document.createElement('td');

      row.setAttribute('data-id', pet.id);
      index.className = "index";
      animal.className = "row-animal";
      name.className = "row-name";
      gender.className = "row-gender";
      age.className = "row-age";

      index.innerText = pet.id;
      animal.innerText = pet.animal;
      name.innerText = pet.name;
      gender.innerText = pet.gender;
      age.innerText = pet.age;
      row.appendChild(buttons);
      row.appendChild(index);
      row.appendChild(animal);
      row.appendChild(name);
      row.appendChild(gender);
      row.appendChild(age);
      return row;
  };

  // const rerenderTable = () => {
  //   getData().then((data) => {
  //     animals = data;
  //     Array.from(tbody.children).forEach(row => row.remove());  // remove tbody childrens if existed to create new updated tbody
  //     createTable(animals);
  //   });
  // };
  const createTable = (data) => {
    data.forEach(pet => {
      let row = createRow(pet);
      tbody.appendChild(row);
    });
  };

  const setFormData = (row) => {
    let result = {};
    row.childNodes.forEach(node => {
      if (node.tagName === "TD" && node.className !== '' && node.className !== 'index') {
        let key = node.className;
        result[key] = node.innerText;
      }
    });
    return result;
  };

  const toggleEditForm = () => {
    form.style.display ='block';
    document.getElementById('form_animal').focus();
  };

  const editRow = (row) => {
    let animal = document.getElementById('form_animal'),
        name = document.getElementById('form_name'),
        gender = document.getElementById('form_gender'),
        age = document.getElementById('form_age');
    let rowData = setFormData(row);

    animal.value = rowData['row-animal'];
    name.value = rowData['row-name'];
    gender.value = rowData['row-gender'];
    age.value = rowData['row-age'];
    // editing = true;
    toggleEditForm();
  };

  const tableButtonClick = (e) => {
    const target = e.target;
    currentRow = e.target.closest('tr');
    const currentRowIndex = +currentRow.getAttribute('data-id');

    if (target.classList.contains("edit")) {
      editRow(currentRow);
    } else if (target.classList.contains("delete")) {
      fetch(`/pets/${currentRowIndex}`, {
        method: 'delete'
      }).then(res => {
        if (res.ok) {
          currentRow.remove();
        }
      });
    }
  };

  const saveRowData = (row = '') => {
    let animalInput = document.getElementById('form_animal').value,
        nameInput = document.getElementById('form_name').value,
        genderInput = document.getElementById('form_gender').value,
        ageInput = document.getElementById('form_age').value;

    let pet = {
      "id": lastId,
      "animal": animalInput,
      "gender": genderInput,
      "name": nameInput,
      "age": ageInput
    };

    if (row === '') { // handle adding new row
      lastId += 1;
      pet.id = lastId;
      fetch(`/pets`, {
        method: 'post',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(pet)
      }).then(res => {
        if (res.ok) {
          row = createRow(pet);
          tbody.appendChild(row);
          lastId++;
          return res.json();
        }
      }).then(data => {
        console.log(data);
      });
    } else { // editing existing row
      row.childNodes.forEach(node => {
        switch (node.className) {
          case 'row-animal': {
            node.innerText = animalInput;
            break;
          }
          case 'row-gender': {
            node.innerText = genderInput;
            break;
          }
          case 'row-name': {
            node.innerText = nameInput;
            break;
          }
          case 'row-age': {
            node.innerText = ageInput;
            break;
          }
          default: {
            return;
          }
        }
      });
      fetch(`/pets/${pet.id}`, {
        method: 'PATCH',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(pet)
      }).then(res => {
        if (res.ok) {
          return res.json();
        }
      }).then(data => {
        console.log(data);
      });
    }
  };

  const formButtonClick = e => {
    e.preventDefault();
    const target = e.target;
    if (target.classList.contains("edit")) {
      saveRowData(currentRow);
      currentRow = '';
      form.style.display = "none";
      form.reset();
    } else if (target.classList.contains("cancel")) {
      currentRow = '';
      form.style.display = "none";
      form.reset();
    }
  };

  const sortColumn = (e) => {
    const target = e.target;
    let tr = Array.prototype.slice.call(tbody.rows, 0);
    let col = +target.getAttribute('id').split('-')[1];

    reverse = -((+reverse) || -1);
    tr = tr.sort(function (a, b) { // sort rows
        a = a.cells[col].textContent; // get cells value
        b = b.cells[col].textContent;

        if (+a) { // for sorting a numbers
          return reverse * (parseInt(a)-parseInt(b)); // use reverse to opposite order
        }
        return reverse * (a.trim()).localeCompare(b.trim());
    });

    for(let i = 0; i < tr.length; ++i) {tbody.appendChild(tr[i]);} // append each row in order
  };

  const getData = () => {
    return new Promise((resolve, reject) => {
      fetch('/pets/data').then(res => {
        if (res.ok) {
          res.json().then(data => {
            resolve(data);
          });
        } else {
          reject("Something went wrong");
        }
      });
    });
  };

  getData().then((data) => {
    animals = data;
    createTable(animals);
    table.addEventListener("click", tableButtonClick);
    form.addEventListener("click", formButtonClick);
    addBtn.addEventListener("click", toggleEditForm);
    header.addEventListener("click", sortColumn);
    lastId = Array.from(document.querySelectorAll("[data-id]")).map(el => parseInt(el.dataset.id, 10)).sort((a,b) => b-a)[0];
  });

});

