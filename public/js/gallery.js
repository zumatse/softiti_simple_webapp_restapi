"use strict";

// const imgUrls = ['https://source.unsplash.com/PC_lbSSxCZE/1024x860','https://source.unsplash.com/lVmR1YaBGG4/800x600','https://source.unsplash.com/5KvPQc1Uklk/800x600','https://source.unsplash.com/GtYFwFrFbMA/800x600','https://source.unsplash.com/Igct8iZucFI/800x600','https://source.unsplash.com/M01DfkOqz7I/800x600','https://source.unsplash.com/MoI_cHNcSK8/800x600','https://source.unsplash.com/M0WbGFRTXqU/800x600','https://source.unsplash.com/s48nn4NtlZ4/800x600','https://source.unsplash.com/E4944K_4SvI/800x600','https://source.unsplash.com/F5Dxy9i8bxc/800x600','https://source.unsplash.com/iPum7Ket2jo/800x600'
// ];

const createElement = (selector, classes ='', id ='') => {
  let newElement = document.createElement(selector);

  if (classes) {
    classes.split(' ').forEach(cl => newElement.classList.add(cl));
  }

  if (id) {
    newElement.setAttribute('id', id);
  }
  return newElement;
};

document.addEventListener("DOMContentLoaded", function() {
  const imgUrls = [];
  let loadedPhotos = 0;
  const getPhotos = () => {
    fetch(`https://pixabay.com/api/?key=7539106-db48cda63bbc50f8bfc0ccac4&q=animals&image_type=photo&pretty=true&per_page=50`).then(response => {
      if (response.ok) {
        response.json().then(json => {
          for (let i = 0; i <= 5; i++) {
            imgUrls.push(json.hits[i+loadedPhotos].webformatURL);
          }

          createGrid();
        });
      }
    });
  };

  const createGrid = () => {
    const tilesSizes = ["medium", "large", "medium", "small", "tall", "wide"];

    let tiles = tilesSizes.map(size => {
      return createElement('div', `gallery-item ${size}`);
    })
    .map((tile, i) => {
      tile.style.backgroundImage = `url('${imgUrls[i+loadedPhotos]}')`;
      return tile;
    });

    tiles.forEach(tile => gallery.appendChild(tile));
    loadedPhotos += 6;
  };

  const showModal = () => {
    backdrop.style.display = 'block';
  };

  const showFullPhoto = (e) => {
    const target = e.target;
    const regexp = new RegExp(/(["'])(.*?[^\\])\1/);
    const url = target.style.backgroundImage.match(regexp)[0].replace(/"/g, '');

    if (target.classList.contains('gallery-item')) {
      let img = createElement('img');
      img.setAttribute('src', url);
      modal.appendChild(img);
      showModal();
    }
  };

  const closeFullPhoto = () => {
    backdrop.style.display = 'none';
    modal.firstChild.remove();
  };

  const addLoadMoreBtn = () => {
    const footer = document.querySelector('footer');
    const btn = createElement('button', undefined, 'load-more');
    btn.innerText = 'Load More';
    footer.insertAdjacentElement('beforebegin', btn);
    btn.addEventListener('click', getPhotos);
  };

  const gallery = createElement('div', undefined, 'gallery');
  const backdrop = document.querySelector('.backdrop');
  const modal = document.querySelector('.modal');
  document.querySelector('main').appendChild(gallery);
  getPhotos();
  addLoadMoreBtn();

  backdrop.addEventListener('click', closeFullPhoto);
  gallery.addEventListener('click', showFullPhoto);

});
