'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PetSchema = new Schema({
  "id": Number,
  "animal": {type: String, required: true},
  "gender": {type: String, required: true},
  "name": {type: String, required: true},
  "age": {type: Number, required: true}
});

module.exports = mongoose.model('Pet', PetSchema);