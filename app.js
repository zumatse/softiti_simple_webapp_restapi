'use strict';
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config  = require('config');
const app = express();

const index = require('./routes/index');
const pets = require('./routes/pets');
const gallery = require('./routes/gallery');

mongoose.Promise = global.Promise;

mongoose.connect(config.DBHost, {
  useMongoClient: true
}).then(() => {
  console.log('MongoDB Connected...');
  console.log(config.DBHost);
})
  .catch(err => console.log(err));


app.use(morgan('dev'));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

app.use(express.static('public'));
app.use('/', index);
app.use('/pets', pets);
app.use('/gallery', gallery);

app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res) => {
  res.status(error.status || 500);
  res.json({
      error: {
          message: error.message
      }
  });
});

module.exports = app;