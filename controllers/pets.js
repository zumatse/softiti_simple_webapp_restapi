'use strict';

let Pet = require('../models/Pet');

// GET /data

const getPetsData = (req, res) => {
  let data;
  Pet.find().then(pets => {
    data = pets;
    res.status(200);
    res.send(data);
  })
  .catch(err => {
    res.status(500).json({error: err});
  });
};

// GET /:id

const getPetById = (req, res) => {
  const id = req.params.id;
  Pet.find({'id': id}).then(data => {
    res.status(200).json({
      message: data
    });
  }).catch(err => {
    res.status(500).json({
      error: err
    });
  });
};

// POST /

const postPet = (req, res) => {
  const pet = new Pet({
    "id": req.body.id,
    "animal": req.body.animal,
    "gender": req.body.gender,
    "name": req.body.name,
    "age": +req.body.age
  });

  pet.save()
  .then(result => {
    console.log(result);
    res.status(200).json({
      message: 'Handling POST requests to /pets',
      createdPet: pet
    });
  })
  .catch(err => {
    console.log(err);
    res.status(206);
    res.send(err);
  });


};

// PATCH /:id

const updatePet = (req, res) => {
  const id = req.params.id;

  Pet.update({'id': id}, {
    "animal": req.body.animal,
    "gender": req.body.gender,
    "name": req.body.name,
    "age": +req.body.age
  })
    .then(result => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
};

// DELETE /:id

const deletePet = (req, res) => {
  const id = req.params.id;
  Pet.remove({'id': id}).then(() => {
    res.status(200).json({
      message: 'Animal deleted from database'
    });
  }).catch(err => {
    res.status(500).json({
      error: err
    });
  });
};

module.exports = {getPetsData, getPetById, postPet, updatePet, deletePet};