"use strict";

module.exports = function(grunt) {
  //Configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      js: {
        options: {
          // Replace all 'use strict' statements in the code with a single one at the top
          banner: "'use strict';\n",
          process: function(src, filepath) {
            return '// Source: ' + filepath + '\n' +
              src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
          }
        },
        src: ['public/js/*.js'],
        dest: 'build/static/js/scripts.js'
      },
      css: {
        src: ['public/css/*.css'],
        dest: 'build/static/css/styles.css'
      }
    },
    postcss: {
      options: {
        processors: [
          require('autoprefixer')({browsers: 'last 2 versions'})
        ]
      },
      dist: {
        src: ['build/static/css/*.css', '!*.min.css']
      }
    },
    cssmin: {
      build: {
        files: [{
          expand: true,
          cwd: 'build/static/css',
          src: ['*.css', '!*.min.css'],
          dest: 'build/static/css',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
      build: {
        files: [{
          src: 'build/static/js/scripts.js',
          dest: 'build/static/js/scripts.min.js'
        }]
      }
    },
    watch: {
      js: {
        files: ['public/js/**/*.js', '*.js'],
        tasks: ['concat']
      },
      css: {
        files: ['public/css/**/*.css'],
        tasks: ['concat']
      }
    }
  });

  // Load plugins
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify-es');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Register tasks

  grunt.registerTask('concat-js', ['concat:js']);
  grunt.registerTask('concat-css', ['concat:css']);
  grunt.registerTask('default', ['concat', 'postcss', 'cssmin', 'uglify']);
};